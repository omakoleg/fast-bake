# fast-bake

Helper scripts for assembling backend typescript project.

Hides all setup steps for `typescript`, `jest`, `prettier`, `eslint` (etc) and have them unified in one place.

## Usage

```sh
$ mkdir my-backend // create new directory
$ cd my-backend
$ npx fast-bake init // run "init" to copy template project
$ yarn build // build project with fast-bake scripts
```

This would bootstrap backend express with `fast-bake` build scripts.

## Installation

Adding to existing codebase:

```sh
yarn add fast-bake
```

Modify `package.json` to execute required scripts:

```json
{
    "scripts": {
        "build": "fast-bake build",
        "test": "fast-bake test",
        "start": "fast-bake start",
        "lint": "fast-bake lint"
    }
}
```

Add required `tsconfig.json` file with contents:

```json
{
    "extends": "fast-bake/configs/tsconfig.json",
    "include": ["src/**/*.ts"]
}
```

## Build script

-   transpile all sources in `src/` folder and write js files into `dist/`
-   `src/index.ts` is required
-   `tsconfig.json` is required (so far)

## Test script

-   Runs `yarn lint` in Check mode to notify about mistakes
-   Runs `jest` for all default test files in `src/` folder in watch mode
-   If `env.CI` is set, watch mode would not be enabled

## Start script

Parallel processes executed:

-   `nodemon` for `dist/` folder and it would run `dist/index.js` with delay
-   `tsc` in watch mode (same as for build step)

## Lint script

Runs `eslint` and `prettier` for `src/` folder and apply style and formatting changes

## Init script

Copies sample project structure from `template` folder into current folder.

## Development

Heavily inspired by `react-scripts`.

Some code approaches reused.
