# express-generated

Template for `fast-bake` backend.

## Scripts

### Build

-   transpile all sources in `src/` folder and write js files into `dist/`
-   `src/index.ts` is required
-   `tsconfig.json` is required (so far)

### Test

-   Runs `yarn lint` in Check mode to notify about mistakes
-   Runs `jest` for all default test files in `src/` folder in watch mode
-   If `env.CI` is set, watch mode would not be enabled

### Start

Parallel processes executed:

-   `nodemon` for `dist/` folder and it would run `dist/index.js` with delay
-   `tsc` in watch mode (same as for build step)

#### Lint

Runs `eslint` and `prettier` for `src/` folder and apply style and formatting changes
