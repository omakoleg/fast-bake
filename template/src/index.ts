import { buildApplication } from './app';
import { createServer } from 'http';

const port = parseInt(process.env.PORT || '3000');
const app = buildApplication();
app.set('port', port);
const server = createServer(app);

server.on('error', (error: any) => {
  if (error.syscall !== 'listen') {
    throw error;
  }
  switch (error.code) {
    case 'EACCES':
      console.error(`Port ${port} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(`Port ${port} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
});
server.on('listening', () => {
  console.log('Listening on ' + port);
});
server.listen(port);

// Handle ^C (-2)
process.on('SIGTERM', () => {
  console.log('Graceful express shutdown');
  server.close(function() {
    console.log('Express backend closed');
  });
});
