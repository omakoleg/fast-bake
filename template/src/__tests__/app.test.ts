import { makeFetch } from 'supertest-fetch';
import { buildApplication } from '../app';

const application = buildApplication();
const fetch = makeFetch(application);

describe('The application', () => {
  it('Index route', async () => {
    await fetch('/')
      .expect(200)
      .expect('content-type', 'application/json; charset=utf-8')
      .expect([
        {
          id: 1,
          username: 'User 1',
        },
        {
          id: 2,
          username: 'User 2',
        },
      ]);
  });
});
