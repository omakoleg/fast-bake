import express, { Request, Response } from 'express';

const router = express.Router();

router.get('/', (_: Request, res: Response) => {
  res.json([
    {
      id: 1,
      username: 'User 1',
    },
    {
      id: 2,
      username: 'User 2',
    },
  ]);
});

export const indexRouter = router;
