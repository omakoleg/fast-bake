import { checkRequiredFiles } from '../utils/checkRequiredFiles';
import { fork } from 'child_process';
import { Paths } from '../utils/paths';
import chalk from 'chalk';
import { cleanDistSync } from '../utils/cleanDist';

export const runBuild = (paths: Paths) => {
  cleanDistSync(paths);
  // Warn and crash if required files are missing
  if (!checkRequiredFiles([paths.index])) {
    process.exit(1);
  }

  console.log(chalk.gray('Creating an optimized production build...'));
  const script = {
    path: paths.binTsc,
    args: ['-p', paths.appTsconfig],
    options: { cwd: paths.appPath, env: { NODE_ENV: 'production' } },
  };

  const compiler = fork(script.path, script.args, script.options);
  compiler.on('exit', code => {
    process.exit(code || 0);
  });

  const exitChildren = () => {
    console.log(chalk.gray('Stopping compiler'));
    compiler.kill('SIGINT');
  };
  process.on('SIGTERM', exitChildren);
  process.on('SIGINT', exitChildren);
};
