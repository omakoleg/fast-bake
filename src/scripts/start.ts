import { checkRequiredFiles } from '../utils/checkRequiredFiles';
import chalk from 'chalk';
import { fork, ChildProcess } from 'child_process';
import { Paths } from '../utils/paths';
import { log } from '../utils/log';
import { cleanDistSync } from '../utils/cleanDist';

export const runStart = (paths: Paths) => {
  cleanDistSync(paths);
  // Warn and crash if required files are missing
  if (!checkRequiredFiles([paths.index])) {
    process.exit(1);
  }

  console.log(chalk.green(`Starting application in watch mode`));
  const scripts = [
    {
      name: 'Application with live reload',
      path: paths.binNodemon,
      args: [
        '--watch',
        paths.appDistFolder,
        '--delay',
        '1000ms',
        '--signal',
        'SIGTERM', // graceful shutdown
        paths.appDistRunnable,
      ],
      options: { cwd: paths.appPath, env: { NODE_ENV: 'development' } },
    },
    {
      name: 'Source files changes watch',
      path: paths.binTsc,
      args: ['-p', paths.appTsconfig, '--watch', '--preserveWatchOutput'],
      options: { cwd: paths.appPath, env: { NODE_ENV: 'development' } },
    },
  ];

  const runningScripts: ChildProcess[] = [];

  scripts.forEach(script => {
    log(`Running: ${script.name} ${script.path} ${script.args.join(' ')}`);
    const runningScript = fork(script.path, script.args, script.options);
    runningScript.on('close', () => log(`Stopped: ${script.name}`));
    runningScripts.push(runningScript);
  });

  const exitChildren = () => {
    log(`Stopping start script`);
    runningScripts.map(s => {
      s.kill('SIGINT');
    });
  };
  process.on('SIGTERM', exitChildren);
  process.on('SIGINT', exitChildren);
};
