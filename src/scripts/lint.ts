import chalk from 'chalk';
import { execSync } from 'child_process';
import { Paths } from '../utils/paths';
import { log } from '../utils/log';

export const runLint = (paths: Paths, writeOrVerify: boolean) => {
  console.log(chalk.green(`Executing formatting checks`));

  const extensions = `{js,ts,json}`;
  log(`Executing prettier`);
  const prettierCommand = [
    paths.binPrettier,
    '--config',
    paths.configsPrettier,
    writeOrVerify ? '--write' : '--check',
    '--ignore-path',
    paths.configsPrettierIgnore,
    `"${paths.appPath}/**/**/**/*.${extensions}"`,
  ].join(' ');
  log(prettierCommand);
  execSync(prettierCommand, { stdio: 'inherit' });

  log(`Executing EsLint`);
  const eslintCommand = [
    paths.binEslint,
    '-c',
    paths.configsEslint,
    '--ext',
    '.ts',
    writeOrVerify ? '--fix' : '',
    paths.appSourcesFolder,
  ].join(' ');
  log(eslintCommand);
  execSync(eslintCommand, { stdio: 'inherit' });
};
