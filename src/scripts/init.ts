import chalk from 'chalk';
import {
  readdirSync,
  statSync,
  existsSync,
  copySync,
  removeSync,
} from 'fs-extra';
import { join } from 'path';
import { execSync } from 'child_process';
import { Paths } from '../utils/paths';

const printFilesTree = (prefix: string, root: string) => {
  readdirSync(root).forEach(file => {
    const currentFullPath = join(root, file);
    const stat = statSync(currentFullPath);
    if (stat && stat.isDirectory()) {
      console.log(chalk.grey(`${prefix}${file}:`));
      printFilesTree(prefix + '| ', currentFullPath);
    } else {
      console.log(chalk.grey(`${prefix}${file}`));
    }
  });
};

export const runInit = (paths: Paths) => {
  if (existsSync(paths.template)) {
    console.log(
      chalk.grey(`Creating new project in: `) + chalk.green(paths.appPath)
    );
    copySync(paths.template, paths.appPath);
    // NPM Issue https://github.com/npm/npm/issues/7252
    // .gitignore would be renamed to .npmignore
    removeSync(`${paths.appPath}/.npmignore`);

    // Make sure to run this before creating ./node_modules
    console.log(chalk.grey(`Added template files:`));
    printFilesTree('\t', paths.appPath);

    console.log(chalk.grey(`Executing Yarn:`));
    execSync(`yarn`, { stdio: 'inherit' });
  } else {
    console.error(
      `Could not locate supplied template: ${chalk.green(paths.template)}`
    );
    return;
  }
};
