import { jestConfig } from '../utils/jestConfig';
import { dirname } from 'path';
import * as jest from 'jest';
import { execSync } from 'child_process';
import resolve from 'resolve';
import { Paths } from '../utils/paths';

function isInGitRepository() {
  try {
    execSync('git rev-parse --is-inside-work-tree', { stdio: 'ignore' });
    return true;
  } catch (e) {
    return false;
  }
}

export const useWatchMode = (argv: string[]): string[] => {
  let resultingArgv = argv.filter(v => v !== '--watch');
  let replaceWith: string | undefined;
  if (!process.env.CI && argv.includes('--watch')) {
    // https://github.com/facebook/create-react-app/issues/5210
    replaceWith = isInGitRepository() ? '--watch' : '--watchAll';
  }
  if (replaceWith) {
    resultingArgv = resultingArgv.concat([replaceWith]);
  }
  return resultingArgv;
};

export const runTest = (paths: Paths, argvInitial: string[]) => {
  let argv = useWatchMode(argvInitial);

  argv.push('--config', JSON.stringify(jestConfig(paths)));

  // This is a very dirty workaround for https://github.com/facebook/jest/issues/5913.
  // We're trying to resolve the environment ourselves because Jest does it incorrectly.
  // TODO: remove this as soon as it's fixed in Jest.
  function resolveJestDefaultEnvironment(name: string) {
    const jestDir = dirname(
      resolve.sync('jest', {
        basedir: __dirname,
      })
    );
    const jestCLIDir = dirname(
      resolve.sync('jest-cli', {
        basedir: jestDir,
      })
    );
    const jestConfigDir = dirname(
      resolve.sync('jest-config', {
        basedir: jestCLIDir,
      })
    );
    return resolve.sync(name, {
      basedir: jestConfigDir,
    });
  }
  const cleanArgv = [];
  let env = 'jsdom';
  let next: string;
  do {
    next = argv.shift() || '';
    if (next === '--env') {
      env = argv.shift() || '';
    } else if (next.indexOf('--env=') === 0) {
      env = next.substring('--env='.length);
    } else {
      cleanArgv.push(next);
    }
  } while (argv.length > 0);
  argv = cleanArgv;
  let resolvedEnv;
  try {
    resolvedEnv = resolveJestDefaultEnvironment(`jest-environment-${env}`);
  } catch (e) {
    // ignore
  }
  if (!resolvedEnv) {
    try {
      resolvedEnv = resolveJestDefaultEnvironment(env);
    } catch (e) {
      // ignore
    }
  }
  const testEnvironment = resolvedEnv || env;
  argv.push('--env', testEnvironment);
  jest.run(argv);
};
