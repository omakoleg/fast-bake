#!/usr/bin/env node

import { runInit } from './scripts/init';
import { runLint } from './scripts/lint';
import { currentPaths } from './utils/paths';
import { runStart } from './scripts/start';
import { runTest } from './scripts/jest';
import { runBuild } from './scripts/build';

process.on('unhandledRejection', err => {
  throw err;
});

const args = process.argv.slice(2);
const allowedCommands = ['build', 'start', 'test', 'lint', 'init'];
const scriptIndex = args.findIndex(v => allowedCommands.includes(v));
const script = scriptIndex === -1 ? args[0] : args[scriptIndex];
const nodeArgs = args.slice(scriptIndex + 1);

switch (script) {
  case 'init':
    runInit(currentPaths('production'));
    break;
  case 'lint':
    runLint(currentPaths('development'), true);
    break;
  case 'start':
    runStart(currentPaths('development'));
    break;
  case 'test':
    const testPaths = currentPaths('test');
    runLint(testPaths, false);
    runTest(testPaths, nodeArgs);
    break;
  case 'build':
    runBuild(currentPaths('production'));
    break;
  default:
    console.log('Unknown script "' + script + '".');
}
