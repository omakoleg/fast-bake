import { Paths } from './paths';

export const jestConfig = (paths: Paths) => ({
  rootDir: paths.appPath,
  roots: ['<rootDir>/src'],
  globals: {
    'ts-jest': {
      tsConfig: paths.configsTsConfig,
    },
  },
  setupFiles: [],
  testPathIgnorePatterns: ['/node_modules/', '/dist/'],
  testMatch: [
    '<rootDir>/src/__tests__/**/*.ts',
    '<rootDir>/src/**/*.{spec}.ts',
  ],
  transform: {
    '^.+\\.(js|ts)$': 'ts-jest',
  },
  // transformIgnorePatterns: [
  //   '[/\\\\]node_modules[/\\\\].+\\.(js|jsx|ts|tsx)$',
  //   '^.+\\.module\\.(css|sass|scss)$',
  // ],
  moduleFileExtensions: ['ts', 'js'],
});
