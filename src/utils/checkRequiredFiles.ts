import { accessSync, constants } from 'fs';
import { dirname, basename } from 'path';
import chalk from 'chalk';

export const checkFileExists = (filePath: string) => {
  try {
    accessSync(filePath, constants.F_OK);
    return true;
  } catch (err) {
    return false;
  }
};

export const checkRequiredFiles = (files: string[]) => {
  for (const filePath of files) {
    if (!checkFileExists(filePath)) {
      const dirName = dirname(filePath);
      const fileName = basename(filePath);
      console.log(chalk.red('Could not find a required file.'));
      console.log(chalk.red('  Name: ') + chalk.cyan(fileName));
      console.log(chalk.red('  Searched in: ') + chalk.cyan(dirName));
      return false;
    }
  }
  return true;
};
