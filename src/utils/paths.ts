import { realpathSync } from 'fs';
import { join, resolve } from 'path';
import { checkFileExists } from './checkRequiredFiles';
import chalk from 'chalk';

// cwd
const appDirectory = realpathSync(process.cwd());
// application
const resolveApp = (relativePath: string) =>
  resolve(appDirectory, relativePath);

// path to 'bin' in installed npm or self checks
const resolveToolsBin = (targetUse: TargetUse, binName: string): string =>
  resolveApp(
    `${
      targetUse === 'application' ? `./node_modules/fast-bake` : '.'
    }/node_modules/.bin/${binName}`
  );

// own root
const resolveOwn = (file: string) => join(__dirname, '..', '..', file);
// own config
const resolveOwnConfig = (file: string) => resolveOwn(join('configs', file));

type Env = 'test' | 'development' | 'production';
type TargetUse = 'application' | 'init' | 'fast-bake';

export const currentPaths = (env: Env): Paths => {
  process.env.NODE_ENV = env;

  const isInstalledAsNpm = checkFileExists(
    resolveApp('./node_modules/fast-bake/package.json')
  );
  const isOwnPackageExists = checkFileExists(resolveApp('./package.json'));
  const use: TargetUse = isOwnPackageExists
    ? isInstalledAsNpm
      ? 'application'
      : 'fast-bake'
    : 'init';
  console.log(chalk.bgCyan(`  Running: ${use}  `));

  return {
    // bins
    binEslint: resolveToolsBin(use, 'eslint'),
    binPrettier: resolveToolsBin(use, 'prettier'),
    binTsc: resolveToolsBin(use, 'tsc'),
    binNodemon: resolveToolsBin(use, 'nodemon'),
    // template
    template: resolveOwn('template'),
    // library
    configsTsConfig: resolveOwnConfig('tsconfig.json'),
    configsEslint: resolveOwnConfig('.eslintrc.js'),
    configsPrettier: resolveOwnConfig('.prettierrc.js'),
    configsPrettierIgnore: resolveOwnConfig('.prettierignore'),
    // application
    appPath: resolveApp('.'),
    index: resolveApp('src/index.ts'),
    appSourcesFolder: resolveApp('src/'),
    appTsconfig: resolveApp('./tsconfig.json'),
    appDistFolder: resolveApp('dist'),
    appDistRunnable: resolveApp('dist/index.js'),
  };
};

export interface Paths {
  binPrettier: string;
  binEslint: string;
  binTsc: string;
  binNodemon: string;
  template: string;
  configsTsConfig: string;
  configsEslint: string;
  configsPrettier: string;
  configsPrettierIgnore: string;
  appPath: string;
  index: string;
  appSourcesFolder: string;
  appDistFolder: string;
  appTsconfig: string;
  appDistRunnable: string;
}
