import { Paths } from './paths';
import { removeSync } from 'fs-extra';
import chalk from 'chalk';
import { log } from './log';

export const cleanDistSync = (paths: Paths) => {
  log(`Cleaning: ${paths.appDistFolder}`);
  removeSync(paths.appDistFolder);
  log(`Removed: ${paths.appDistFolder}`);
  console.log(chalk.gray(`Dist folder removed`));
};
